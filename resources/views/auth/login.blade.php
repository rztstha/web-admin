<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Project - Admin & Dashboard </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('super-admin/images/favicon.ico') }}">
    <!-- App css -->
    <link href="{{ asset('super-admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('super-admin/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('super-admin/css/app.min.css') }}" rel="stylesheet" type="text/css" />
</head>

<body class="account-body accountbg">
    <!-- Log In page -->
    <div class="container">
        <div class="row vh-100 d-flex justify-content-center">
            <div class="col-12 align-self-center">
                <div class="row">
                    <div class="col-lg-4 mx-auto">
                        <div class="card">
                            <div class="card-body p-0 auth-header-box">
                                <div class="text-center p-3">
                                    <h4 class="mt-3 mb-1 fw-semibold text-white font-18">Sign In</h4>
                                    <p class="text-muted  mb-0">Sign in to continue.</p>
                                </div>
                            </div>
                            <div class="card-body p-4">
                                <form method="POST" action="{{ route('login.custom') }}">
                                    @csrf

                                    <div class="form-group mb-2">
                                                    <label class="form-label" for="username">Username</label>
                                                    <div class="input-group">                                                                                         
                                                    <input type="text" placeholder="Email" id="email" class="form-control" name="email" required autofocus>
                                        @if ($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                        @endif                                                    </div>                                    
                                                </div><!--end form-group--> 
                    
                                                <div class="form-group mb-2">
                                                    <label class="form-label" for="userpassword">Password</label>                                            
                                                    <div class="input-group">                                  
                                                    <input type="password" placeholder="Password" id="password" class="form-control" name="password" required>
                                        @if ($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @endif                                                    </div>                               
                                                </div><!--end form-group--> 
                    
                                                <div class="form-group row my-3">
                                                    <div class="col-sm-6">
                                                        <div class="custom-control custom-switch switch-success">
                                                            <input type="checkbox" class="custom-control-input" id="customSwitchSuccess">
                                                            <label class="form-label text-muted" for="customSwitchSuccess">Remember me</label>
                                                        </div>
                                                    </div><!--end col--> 
                                                    
                                                </div><!--end form-group--> 
                    
                                                <div class="form-group mb-0 row">
                                                    <div class="col-12">
                                                        <button class="btn btn-primary w-100 waves-effect waves-light" type="submit">Log In <i class="fas fa-sign-in-alt ms-1"></i></button>
                                                    </div><!--end col--> 
                                                </div> <!--end form-group--> 
                                    
                                   
                                   
                                </form>
                            </div>
                        </div>
                        <div class="card-body bg-light-alt text-center">
                            <span class="text-muted d-none d-sm-inline-block">
                                admin © <script>
                                    document.write(new Date().getFullYear())
                                </script>
                            </span>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end col-->
    </div>
    <!--end row-->
    </div>
    <!--end container-->
    <!-- End Log In page -->
    <!-- jQuery  -->
    <script src="{{ asset('super-admin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('super-admin/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('super-admin/js/waves.js') }}"></script>
    <script src="{{ asset('super-admin/js/feather.min.js') }}"></script>
    <script src="{{ asset('super-admin/js/simplebar.min.js') }}"></script>
</body>

</html>