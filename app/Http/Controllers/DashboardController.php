<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;

use App\Models\dashboard;

class DashboardController extends Controller
{
 
/* Controller Method */
public function index() {
    $users = User::get()->count();
    return view('super-admin.dashboard.dashboard', compact('users'));
}


}
